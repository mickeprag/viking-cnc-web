---
layout: page
title: Help
subtitle: Support this project
permalink: /support.html
---

The best way to support me and this project is by using the links on this site when ordering part. When using the links
I will get a small kickback from the order. And the best part, it will not cost you anyting extra! It's a win-win.

You don't even need to order the parts linked, you can order anyting on the site. Just make sure you press a link from this site first.

You can also use this link directly:
<a href="https://www.banggood.com?p=8M01165684558201608R&custlinkid=196579" target="_blank">Banggood</a>
