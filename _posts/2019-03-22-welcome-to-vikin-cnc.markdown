---
layout: post
title:  "Welcome to Viking CNC!"
date:   2019-03-22 21:20:14 +0100
categories: cnc
---

Domain registered, check. Web site deployed, check. CNC build in progress, check!
This project is now officially live.

This is the home of the Viking CNC machine. Here you will find plans, guides and instructions for how you can build your own
CNC machine. When you are successful, stick around and help us improve the machine to be even better!
