---
layout: page
---

<div class="post-content">

<h2>Bill of materials</h2>

<table>
	<thead>
		<tr>
			<th>Qty.</th>
			<th colspan=2>Component</th>
		</tr>
	</thead>
	<tbody>
{% for part in site.data[page.product][page.assembly].parts %}
		<tr>
			<td>{{ part.qty }}</td>
			<td>
				{% if site.data.items[part.item].img %}
					<img src="{{ site.data.items[part.item].img }}" alt="{{ site.data.items[part.item].desc }}" style="max-height: 40px; max-width: 40px">
				{% endif %}
			</td>
			<td>
				{{ site.data.items[part.item].desc }}
			</td>
		</tr>
{% endfor %}
	</tbody>
</table>

{{ content }}

<h2>3D model</h2>

<iframe src="{{ page.f3d }}" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

</div>
