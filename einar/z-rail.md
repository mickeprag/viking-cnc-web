---
layout: assembly
f3d: https://myhub.autodesk360.com/ue2d2fecd/shares/public/SH7f1edQT22b515c761e0889b8deef9a00f9?mode=embed
product: einar
assembly: z-rail
---

This part holds the Z-Slider and is mounted on the X-Slider.

Build this part together with the X-Slider.
