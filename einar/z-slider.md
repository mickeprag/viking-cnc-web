---
title: Z-Slider
layout: assembly
f3d: https://myhub.autodesk360.com/ue2d2fecd/shares/public/SH7f1edQT22b515c761e5e2cd09eea86e458?mode=embed
product: einar
assembly: z-slider
---

This part is used to hold the tool/spindle. The predrilled holes are suitable for these tools:

- <a target='_blank' href='https://www.banggood.com/ER11-Chuck-CNC-500W-Spindle-Motor-with-52mm-Clamps-and-Power-Supply-Speed-Governor-p-1027937.html?p=8M01165684558201608R&custlinkid=195441'><img src='https://img.staticbg.com/images/oaupload/banggood/images/B6/F1/35357485-8dcd-4b67-ae6b-422e3cc62b9a.jpg' alt='Spindle' style="max-height: 60px; max-width: 60px">&nbsp;Spindle</a>
- <a target='_blank' href='https://www.banggood.com/EleksMaker-LA03-2300-445nm-2300mW-Blue-Laser-Module-2-2-54-2P-TTL-PWM-Modulation-for-DIY-Laser-Engraver-p-1105563.html?p=8M01165684558201608R&custlinkid=195445'><img src='https://img.staticbg.com/images/oaupload/banggood/images/DB/E9/76b96219-150b-4f0a-ad76-38bd0f057ffa.jpg' alt='Laser' style="max-height: 60px; max-width: 60px">&nbsp;Laser</a>

## Step 1: Attach the spacers

<img src="/einar/img/z-slider-step-1-front.png" alt="Z-Slider spacers front" style="max-width: 300px" />
<img src="/einar/img/z-slider-step-1-back.png" alt="Z-Slider spacers back" style="max-width: 300px" />

Attach the spacers to the plate using **M3 8mm** screws.

## Step 2: Add washers and wheel

<img src="/einar/img/z-slider-step-2-1.png" alt="Z-Slider washers" style="max-width: 300px" />
<img src="/einar/img/z-slider-step-2-2.png" alt="Z-Slider wheels" style="max-width: 300px" />

On each spacer, add 5 **M5 washers** then a **wheel** followed by another
**M5 washer** and lastly one **M3 washer**. Fasten using one **M3 8mm** screw.
