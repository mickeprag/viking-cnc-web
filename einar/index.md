---
layout: page
title: Einar (v1)
---

## Goals

Einar is the first model of Viking CNC. The design goal of the machine is:

- Affordable. This machine cost under $300 to build\*.
- Easy to build. No special parts. No 3d printed parts. Only plates that can be cut using common available tools.
- Easy to source parts. The parts should be easy purchased. All parts can be ordered from one supplier.
- Easily modifiable. Make it bigger. Create new tools. Change something else...
- Open. All files are free and open to download and/or modify.

\* Not including spindle or shipping

## How to build

The aim for this project is that it should be easy to build a Viking CNC. Do you already have access to a CNC machine? Awesome, the you can machine all the plates needed. No access to a CNC machine? No problem! You can build it anyway.
If you have a 3d-printer, then print the needed parts on that one. If you only have access to common woodworking tools, you can still build it. Print the templates on your office printer and use your drill and saw to cut the plates. Once you have a machine running recut the parts on the machine to improve the precision.

That is how I started. I made a very basic, weak, and small machine. Just enough to be able to machine the needed parts for the Viking CNC.

## 3d model

Feel free to spin around and look through the machine using this 3d-model. Here you can see how everyting is put together.
<iframe src="https://myhub.autodesk360.com/ue2d2fecd/shares/public/SH7f1edQT22b515c761ee1e6d5e4151cf190?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>
