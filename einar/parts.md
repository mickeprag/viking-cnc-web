---
layout: page
title: Einar (v1)
subtitle: Parts
---

Einar is divided into 6 parts.

## [Z-Slider]({% link einar/z-slider.md %})

This is the part where the router is mounted.

<img src="/einar/z-slider.png" />

## [Z-Rail]({% link einar/z-rail.md %})

This is the part where the router is mounted.

## [X-Slider]({% link einar/x-slider.md %})

This is the moving part for the x-rail where the z-rail is mounted.

## [X-Rail]({% link einar/x-rail.md %})

This is the x gantry.

## [Base]({% link einar/base.md %})

This is the base.
