---
title: X-Rail
layout: assembly
f3d: https://myhub.autodesk360.com/ue2d2fecd/shares/public/SH56a43QTfd62c1cd96877537c5688a47f0d?mode=embed
product: einar
assembly: x-rail
---

This part is the x gantry and holds the z-rail.
