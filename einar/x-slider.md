---
title: X-Slider
layout: assembly
f3d: https://myhub.autodesk360.com/ue2d2fecd/shares/public/SH56a43QTfd62c1cd968e93796ea2f09e012?mode=embed
product: einar
assembly: x-slider
---

This part holds the x-rail and slides on the x-rail.
